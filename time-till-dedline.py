import re
from datetime import datetime

user_input_regex = re.compile("^[a-zA-Z ]*:[0-9]*.[0-9]*.[0-9]*$")


def parse_user_input(input_string):
    match = user_input_regex.match(input_string)
    if match is None:
        raise Exception("Invalid input format")
    user_input_data = input_string.split(":")
    task_name = user_input_data[0]
    try:
        task_deadline = datetime.strptime(user_input_data[1], '%d.%m.%Y').date()
    except ValueError:
        raise Exception("Invalid date")
    current_date = datetime.now().date()
    if current_date >= task_deadline:
        raise Exception("Times already expired")
    return {"deadline": task_deadline, "task_name": task_name}


def calculate_days_to_date(deadline):
    current_date = datetime.now().date()
    date_difference = deadline - current_date
    return date_difference.days


def format_response(days_to_deadline, task_name):
    return f"Time remaining for your task: {task_name} is {days_to_deadline} days!"


try:
    user_input = input("Enter task name and deadline in format {task name}:{dd.MM.yyyy}\n")
    user_input_directory = parse_user_input(user_input)
    remaining_days = calculate_days_to_date(user_input_directory["deadline"])
    response = format_response(remaining_days, user_input_directory["task_name"])
    print(response)
except Exception as ex:
    print(f"Exception occurred: {ex}")
