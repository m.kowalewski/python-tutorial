import re

units_per_day = {
    "hours": 24,
    "minutes": 24 * 60,
    "seconds": 24 * 60 * 60
}

user_input_regex = re.compile("^[0-9]*:[a-z]*$")


def days_to_units(data_to_convert):
    number_of_days = data_to_convert["days"]
    conversion_unit = data_to_convert["unit"]
    result = number_of_days * units_per_day[conversion_unit]
    return f"{number_of_days} days are {result} {conversion_unit}"


def parse_user_input(user_input):
    match = user_input_regex.match(user_input)
    if match is None:
        raise Exception("Invalid input format")
    days_and_unit = user_input.split(":")
    days_and_unit_dictionary = {"days": int(days_and_unit[0]), "unit": days_and_unit[1]}
    if days_and_unit_dictionary["days"] == 0:
        raise Exception("Number of days must be positive")
    if units_per_day[days_and_unit_dictionary["unit"]] is None:
        keys = units_per_day.keys()
        supported_types = ", ".join(keys)
        raise Exception(f"Unsupported unit type!\nSupported unit types: {supported_types}\n")
    return days_and_unit_dictionary