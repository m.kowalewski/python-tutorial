from units_converter import parse_user_input
from units_converter import days_to_units

while True:
    user_input = input("Enter a list of numbers of days and conversion unit in format: {numer_of_days}:{units}\n")
    if user_input == "exit":
        break
    try:
        data_to_convert = parse_user_input(user_input)
        message = days_to_units(data_to_convert)
        print(message)
    except Exception as ex:
        print(f"Exception occurred: {ex}")
