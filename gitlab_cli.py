import requests

from gitlab_project import GitlabProject

response = requests.get("https://gitlab.com/api/v4/users/m.kowalewski/projects")

projects = []

for projest in response.json():
    projects.append(GitlabProject(projest))

for project in projects:
    project.print()
