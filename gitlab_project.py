class GitlabProject:
    def __init__(self, gitlab_project_json):
        self.id = gitlab_project_json['id']
        self.name = gitlab_project_json['name']
        self.url = gitlab_project_json['web_url']

    def print(self):
        print(f"Project name: {self.name}\nProject url: {self.url}\n")